module tb_adder();
	reg [3:0] a;
	reg [3:0] b;
	wire [3:0] sum;

	adder dut(a,b,sum,carry);

	initial begin
		$display("time\t a\t b\t sum\t carry\t");
		$monitor("%g\t %b\t %b\t %b\t %b", $time, a, b, sum, carry);
		a <= 4'b0010; b <= 4'b1010;
		#5 $finish;
	end
endmodule	
