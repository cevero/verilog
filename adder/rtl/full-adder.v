module full_adder(a,b,cin,sum,carry);
	input a,b,cin;
	output sum,carry;

	wire n_sum,n_carry1,n_carry2;

	half_adder U0(.a(a), .b(b), .sum(n_sum), .carry(n_carry1));
	half_adder U1(.a(n_sum), .b(cin), .sum(sum), .carry(n_carry2));
	or U2(carry,n_carry1,n_carry2);
endmodule	
