module adder(a,b,sum,carry);
	input [3:0] a;
	input [3:0] b;
	output [3:0] sum;
	output carry;

	wire [2:0] s;

	full_adder U0(a[0],b[0],1'b0,sum[0],s[0]);
	full_adder U1(a[1],b[1],s[0],sum[1],s[1]);
	full_adder U2(a[2],b[2],s[1],sum[2],s[2]);
	full_adder U3(a[3],b[3],s[2],sum[3],carry);
endmodule	
