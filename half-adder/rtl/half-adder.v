module half_adder(a,b,sum,carry);	//módulo de topo
	input a,b;											//entradas 
	output sum,carry;								//saídas
	xor U0(sum,a,b);
	and U1(carry,a,b);
endmodule	
