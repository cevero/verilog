module testbench();
	reg a,b;

	half_adder dut(a,b,sum,carry);

	initial begin
		$dumpfile("build/wave.vcd");
		$dumpvars(0, testbench);
		$display("time\t a\t b\t sum\t carry\t");
		$monitor("%g\t %b\t %b\t %b\t %b", $time, a, b, sum, carry);
		a <= 0;
		b <= 0;
		#5 a <= 1;
		#5 a <= 0; b <= 1; 
		#5 a <= 1;
		#5 $finish;
	end	
endmodule
