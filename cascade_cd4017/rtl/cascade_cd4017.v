module cascade_cd4017(
  input clk,
  input reset,
  output [16:0] out
);

  wire clk1, reset0, reset1, ce0;
  wire [9:0] out0;
  wire [9:0] out1;

  cd4017 counter0(clk, ce0, reset0, out0,  );
  cd4017 counter1(clk1, 1'b0, reset1, out1,  );

  assign out = {out0[8:0], out1[8:1]};
  assign ce0 = out0[9];
  assign clk1 = out0[9] & clk;
  assign reset0 = reset | out1[9];
  assign reset1 = out0[0];

endmodule

