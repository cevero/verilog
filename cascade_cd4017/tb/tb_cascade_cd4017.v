module tb_cd4017;
  reg clk, reset;
  wire [16:0] out; 

  cascade_cd4017 uut(
    clk,
    reset,
    out
  );

  initial begin
    clk = 1'b0;
    reset = 1'b1;
    forever #5 clk = ~clk;
  end

  initial begin
    $dumpfile("wave.vcd");
    $dumpvars(0, tb_cd4017);
    //$display("time\t in\t out");
    //$monitor("%g\t %h\t %b", $time, in, out);
    #10 reset = 1'b0;
    #200 $finish;
  end

endmodule
