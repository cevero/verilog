module datapath(
	input clk,
	input mem_write,
	input mem_read,
	input acc_write,
	input acc_src,
	input jmp_cond,
	input jmp_uncond,
	input [1:0] alu_op,
	output [2:0] opcode
);

 	wire [12:0] pc_i;
	wire pc_src;
	wire [12:0] pc_o;
	wire [12:0] sum_pc_o;
	wire [15:0] acc_i;
	wire [15:0] acc_o;
	wire [15:0] im_o;
	wire [15:0] alu_o;
	wire [15:0] read_data;
	
	assign opcode = im_o[15:13];
	assign pc_i = (pc_src) ? im_o[12:0] : sum_pc_o;
	assign acc_i = (acc_src) ? read_data : alu_o;
  assign acc_zero = (acc_o == 16'd0) ? 1'b0 : 1'b1;
  assign pc_src = (acc_zero & jmp_cond) | jmp_uncond;

	pc PC(
		.data(pc_i),
		.clk(clk),
		.Q(pc_o)
	);

	sum_pc SUM_PC(
		.data_i(pc_o),
		.data_o(sum_pc_o)
	);

	instruction_memory IR(
		.addr(pc_o),
		.instruction(im_o)
	);

	data_memory DM(
		.write_data(acc_o),
		.addr(im_o[12:0]),
		.mem_write(mem_write),
		.mem_read(mem_read),
		.read_data(read_data),
	);
 
	acc ACC(
		.data(acc_i),
		.clk(clk),
		.acc_write(acc_write),
		.Q(acc_o)
	);
  
	alu ALU(
		.A(acc_o),
		.B(read_data),
		.ALU_Sel(alu_op),
		.ALU_Out(alu_o)
	);
endmodule
