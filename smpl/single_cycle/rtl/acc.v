module acc(
	input [15:0] data,
	input clk,
	input acc_write,
	output reg [15:0] Q
);

always @(posedge clk)
	if (acc_write == 1'b1)
		Q <= data;
endmodule
