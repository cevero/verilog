module data_memory
#(
	parameter ADR   = 13,
	parameter WIDTH = 16,
	parameter DPTH  = 8192
)
(
	input  [WIDTH-1:0] write_data,
	input  [ADR-1:0] addr,
	input mem_write, 
	input mem_read,
	output reg [WIDTH-1:0] read_data
);

	reg [WIDTH-1:0] DM [DPTH-1:0];

	always @ (*) begin
    DM[0] = 16'd1;
    DM[1] = 16'd0;
		if (mem_write == 1'b1 && mem_read == 1'b0) 
			DM [addr] = write_data;
		else if (mem_read == 1'b1 && mem_write == 1'b0)
			read_data = DM [addr]; 
  end    
endmodule
