module controller(
	input [2:0] opcode,
	output reg mem_write,
	output reg mem_read,
	output reg acc_write,
	output reg acc_src,
	output reg jmp_cond,
	output reg jmp_uncond,
	output reg [1:0] alu_op
);

	localparam ADD = 3'b000;
	localparam SUB = 3'b001;
	localparam AND = 3'b010;
	localparam NOT = 3'b011;
	localparam LDA = 3'b100;
	localparam STA = 3'b101;
	localparam JMP = 3'b110;
	localparam JZ = 3'b111;

always @ (*) begin
	case(opcode)
		ADD: begin
			mem_read <= 1'b1;
			mem_write <= 1'b0;
			acc_write <= 1'b1;
			acc_src <= 1'b0;
			alu_op <= 2'b00;
			jmp_uncond <= 1'b0;
			jmp_cond <= 1'b0;
		end
		SUB: begin
			mem_read <= 1'b1;
			mem_write <= 1'b0;
			acc_write <= 1'b1;
			acc_src <= 1'b0;
			alu_op <= 2'b01;
			jmp_uncond <= 1'b0;
			jmp_cond <= 1'b0;
		end
		AND: begin
			mem_read <= 1'b1;
			mem_write <= 1'b0;
			acc_write <= 1'b1;
			acc_src <= 1'b0;
			alu_op <= 2'b10;
			jmp_uncond <= 1'b0;
			jmp_cond <= 1'b0;
		end
		NOT: begin
			mem_read <= 1'b1;
			mem_write <= 1'b0;
			acc_write <= 1'b1;
			acc_src <= 1'b0;
			alu_op <= 2'b11;
			jmp_uncond <= 1'b0;
			jmp_cond <= 1'b0;
		end
		LDA: begin
			mem_read <= 1'b1;
			mem_write <= 1'b0;
			acc_write <= 1'b1;
			acc_src <= 1'b1;
			alu_op <= 2'bxx;
			jmp_uncond <= 1'b0;
			jmp_cond <= 1'b0;
		end
		STA: begin
			mem_read <= 1'b0;
			mem_write <= 1'b1;
			acc_write <= 1'b0;
			acc_src <= 1'bx;
			alu_op <= 2'bxx;
			jmp_uncond <= 1'b0;
			jmp_cond <= 1'b0;
		end
		JMP: begin
			mem_read <= 1'b0;
			mem_write <= 1'b0;
			acc_write <= 1'b0;
			acc_src <= 1'bx;
			alu_op <= 2'bxx;
			jmp_uncond <= 1'b1;
			jmp_cond <= 1'b0;
		end
		JZ: begin
			mem_read <= 1'b0;
			mem_write <= 1'b0;
			acc_write <= 1'b0;
			acc_src <= 1'bx;
			alu_op <= 2'bxx;
			jmp_uncond <= 1'b0;
			jmp_cond <= 1'b1;
		end
	endcase	
end	
endmodule
