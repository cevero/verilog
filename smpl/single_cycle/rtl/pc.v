module pc(data,clk,Q);
	input [12:0] data;
	input clk;
	output reg [12:0] Q;

	always @(posedge clk)
		Q <= data;
endmodule
