module instruction_memory
#(
	parameter ADR   = 13,
	parameter WIDTH = 16
)
(
	input  [ADR-1:0] addr,
	output reg [WIDTH-1:0] instruction
);

  // program
	always @ (*) 
    case(addr)
      13'd0: instruction <= 16'b1000000000000000; 
      13'd1: instruction <= 16'b0000000000000001; 
      13'd2: instruction <= 16'b1010000000000010; 
      13'd3: instruction <= 16'b1000000000000000; 
      13'd4: instruction <= 16'b0000000000000010; 
      13'd5: instruction <= 16'b1100000000000010; 
    endcase	
endmodule
