module smpl(
	input clk
);

	wire mem_write;
	wire mem_read;
	wire acc_write;
	wire acc_src;
	wire jmp_cond;
	wire jmp_uncond;
	wire [1:0] alu_op;
	wire [2:0] opcode;

	controller UC(
		.opcode(opcode),
		.mem_write(mem_write),
		.mem_read(mem_read),
		.acc_write(acc_write),
		.acc_src(acc_src),
		.jmp_cond(jmp_cond),
		.jmp_uncond(jmp_uncond),
		.alu_op(alu_op)
	);	

	datapath DP(
		.clk(clk),
		.mem_write(mem_write),
		.mem_read(mem_read),
		.acc_write(acc_write),
		.acc_src(acc_src),
		.jmp_cond(jmp_cond),
		.jmp_uncond(jmp_uncond),
		.alu_op(alu_op),
		.opcode(opcode)
	);	
endmodule
