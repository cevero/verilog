module tb_smpl;
  reg clk;

  smpl proc(
    .clk(clk)
  );

  always #5 clk = ~clk;

  initial begin
    $dumpvars(0, tb_smpl);
    $dumpfile("build/wave.vcd");
    clk = 1'b0;
    #300 $finish;
  end
endmodule
