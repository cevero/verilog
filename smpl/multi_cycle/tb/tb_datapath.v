module tb_datapath;
  reg clk;
  reg pc_write_cond;
  reg pc_write_uncond;
  reg pc_src;
  reg i_or_d;
  reg mem_read;
  reg mem_write;
  reg ir_write;
  reg acc_write;
  reg acc_src;
  reg src_a;
  reg src_b;
  reg [1:0] alu_op;
  wire [2:0] opcode;

  always #5 clk = ~clk;

  initial begin
    $dumpfile("build/wave.vcd");
    $dumpvars(0, tb_datapath);
    clk = 1'b0;
    pc_write_cond = 1'b0;
    pc_write_uncond = 1'b1;
    pc_src = 1'b0;
    i_or_d = 1'b0;
    mem_read = 1'b1;
    mem_write = 1'b0;
    ir_write = 1'b1;
    acc_write = 1'b0;
    acc_src = 1'b0;
    src_a = 1'b0;
    src_b = 1'b1;
    alu_op = 2'b0;
    #100 $finish;
  end
endmodule
