module register
#(
  parameter WIDTH=16
)
(
  input clk,
  input en,
  input [WIDTH-1:0] data_in,
  output reg [WIDTH-1:0] data_out = 0
);

  always @(posedge clk) begin
    if(en)
      data_out = data_in;
  end
endmodule
