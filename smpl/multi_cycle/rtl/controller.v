module controller(
  input clk,
	input [2:0] opcode,
  output reg pc_write_cond,
  output reg pc_write_uncond,
  output reg pc_src,
  output reg i_or_d,
  output reg mem_read,
  output reg mem_write,
  output reg ir_write,
  output reg acc_write,
  output reg acc_src,
  output reg src_a,
  output reg src_b,
  output reg [1:0] alu_op
);

reg [2:0] state = 3'd0;

localparam ADD = 3'b000;
localparam SUB = 3'b001;
localparam AND = 3'b010;
localparam NOT = 3'b011;
localparam LDA = 3'b100;
localparam STA = 3'b101;
localparam JMP = 3'b110;
localparam JZ = 3'b111;

always @(posedge clk)
  case(state)
    3'd0: state <= 3'd1;        //instruction fetch 
    3'd1:                       //decode
      case(opcode)
        ADD: state <= 3'd2;
        SUB: state <= 3'd2;
        AND: state <= 3'd2;
        NOT: state <= 3'd2;
        LDA: state <= 3'd3;
        STA: state <= 3'd4;
        JMP: state <= 3'd5;
        JZ: state <= 3'd6;
        default: state <= 3'd0;
      endcase  
    3'd2: state <= 3'd0;     //arithmetic-logical
    3'd3: state <= 3'd0;     //LDA
    3'd4: state <= 3'd0;     //STA
    3'd5: state <= 3'd0;     //JMP
    3'd6: state <= 3'd0;     //JZ
  endcase  

always @ (state)
	case(state)
		3'd0: begin
			i_or_d <= 1'b0;
			mem_read <= 1'b1;
			mem_write <= 1'b0;
			ir_write <= 1'b1;
			acc_src <= 1'b0;
			src_a <= 1'b0;
			src_b <= 1'b1;
			alu_op <= 2'b00;
			pc_src <= 1'b1;
			pc_write_uncond <= 1'b1;
		end
    3'd1: pc_write_uncond <= 0;
		3'd2: begin
			i_or_d <= 1'b1;
			mem_read <= 1'b1;
			src_a <= 1'b1;
			src_b <= 1'b0;
			alu_op <= opcode[1:0];
			pc_src <= 1'b0;
			pc_write_uncond <= 1'b1;
		end
		3'd3: begin
			i_or_d <= 1'b1;
			mem_read <= 1'b1;
      acc_src <= 1'b1;
      acc_write <= 1'b1;
		end
		3'd4: begin
			i_or_d <= 1'b1;
			mem_read <= 1'b1;
		end
		3'd5: begin
			pc_src <= 1'b0;
			pc_write_uncond <= 1'b1;
		end
		3'd6: begin
			pc_src <= 1'b0;
			pc_write_cond <= 1'b1;
		end
	endcase	
endmodule
