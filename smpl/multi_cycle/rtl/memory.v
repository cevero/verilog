module memory
#(
	parameter ADR   = 13,
	parameter WIDTH = 16,
	parameter DPTH  = 8192
)
(
	input  [WIDTH-1:0] write,
	input  [ADR-1:0] addr,
	input mem_write, 
	input mem_read,
	output reg [WIDTH-1:0] read
);

	reg [WIDTH-1:0] IDM [DPTH-1:0];

	always @ (*) begin
    IDM[0] <= 16'b1000000000000110; 
    IDM[1] <= 16'b0000000000000111; 
    IDM[2] <= 16'b1010000000001000; 
    IDM[3] <= 16'b1000000000000110; 
    IDM[4] <= 16'b0000000000001000; 
    IDM[5] <= 16'b1100000000000010; 
    IDM[6] <= 16'd1;
    IDM[7] <= 16'd0;
		if (mem_write == 1'b1 && mem_read == 1'b0) 
			IDM [addr] = write;
		else if(mem_read == 1'b1 && mem_write == 1'b0)
			read = IDM [addr];
  end    
endmodule
