module datapath(
  input clk,
  input pc_write_cond,
  input pc_write_uncond,
  input pc_src,
  input i_or_d,
  input mem_read,
  input mem_write,
  input ir_write,
  input acc_write,
  input acc_src,
  input src_a,
  input src_b,
  input [1:0] alu_op,
  output [2:0] opcode
);
  
  wire pc_write;
  wire acc_zero;
  wire [12:0] pc_in;
  wire [12:0] pc_out;
  wire [12:0] i_or_d_out;
  wire [15:0] mem_out;
  wire [15:0] ir_out;
  wire [15:0] acc_out;
  wire [15:0] acc_in;
  wire [15:0] src_a_out;
  wire [15:0] src_b_out;
  wire [15:0] alu_out;

  assign i_or_d_out = (i_or_d) ? ir_out[12:0] : pc_out;
  assign pc_in = (pc_src) ? alu_out : ir_out[12:0];
  assign acc_in = (acc_src) ? mem_out : alu_out;
  assign src_a_out = (src_a) ? acc_out : {3'b000,pc_out};
  assign src_b_out = (src_b) ? 16'd1 : mem_out;
  assign acc_zero = (acc_out == 16'd0) ? 1'b0 : 1'b1;
  assign pc_write = (acc_zero & pc_write_cond) | pc_write_uncond;
  assign opcode = ir_out[15:13];

  register #(.WIDTH(13)) pc(
    .clk(clk),
    .en(pc_write),
    .data_in(pc_in),
    .data_out(pc_out)
  );

  register #(.WIDTH(16)) ir(
    .clk(clk),
    .en(ir_write),
    .data_in(mem_out),
    .data_out(ir_out)
  );

  register #(.WIDTH(16)) acc(
    .clk(clk),
    .en(acc_write),
    .data_in(acc_in),
    .data_out(acc_out)
  );

  alu alu1(
    .a(src_a_out),
    .b(src_b_out),
    .sel(alu_op),
    .out(alu_out)
  );

  memory mem1(
    .write(acc_out),
    .addr(i_or_d_out),
    .mem_write(mem_write), 
    .mem_read(mem_read),
    .read(mem_out)
  );
endmodule
