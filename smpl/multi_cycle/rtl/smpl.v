module smpl(
  input clk
);

  wire pc_write_cond;
  wire pc_write_uncond;
  wire pc_src;
  wire i_or_d;
  wire mem_read;
  wire mem_write;
  wire ir_write;
  wire acc_write;
  wire acc_src;
  wire src_a;
  wire src_b;
  wire [1:0] alu_op;
  wire [2:0] opcode;

  controller UC(
    .clk(clk),
    .opcode(opcode),
    .pc_write_cond(pc_write_cond),
    .pc_write_uncond(pc_write_uncond),
    .pc_src(pc_src),
    .i_or_d(i_or_d),
    .mem_read(mem_read),
    .mem_write(mem_write),
    .ir_write(ir_write),
    .acc_write(acc_write),
    .acc_src(acc_src),
    .src_a(src_a),
    .src_b(src_b),
    .alu_op(alu_op)
  );

  datapath DP(
    .clk(clk),
    .pc_write_cond(pc_write_cond),
    .pc_write_uncond(pc_write_uncond),
    .pc_src(pc_src),
    .i_or_d(i_or_d),
    .mem_read(mem_read),
    .mem_write(mem_write),
    .ir_write(ir_write),
    .acc_write(acc_write),
    .acc_src(acc_src),
    .src_a(src_a),
    .src_b(src_b),
    .alu_op(alu_op),
    .opcode(opcode)
  );
endmodule  
