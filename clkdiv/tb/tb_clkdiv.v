module tb_clkdiv();
  reg  clk, en, rst;
  wire clk_out;

  clkdiv #(.DIVIDER(51),
           .COUNTER_WIDTH(6))
           dut(clk, en, rst, clk_out);

  initial
    begin
      clk = 0;
      rst = 1;
      en = 0;
      forever #1 clk = ~clk;
    end

  initial
    begin
      $dumpfile("build/wave.vcd");
      $dumpvars(0, tb_clkdiv);
      $display("time\t in\t out");
      $monitor("%g\t  %b\t %b", $time, clk, clk_out);
      #5 rst = 0;
      #5 en = 1;

      #400 $finish;
    end
endmodule	
