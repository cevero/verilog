module clkdiv(
    input clk, en, rst,
    output reg clk_out
);

    parameter DIVIDER = 1;
    parameter COUNTER_WIDTH = 1;

    reg [COUNTER_WIDTH:0] counter;

    always @(posedge clk) begin
        if (rst)
            begin
                counter = 26'd0;
                clk_out = 1'b0;
            end
        else if ((counter >= ((DIVIDER >> 1)-1)) && en)
            begin
                clk_out = ~clk_out;
                counter = 26'd0;
            end
        else if (en)
            counter = counter + 26'd1;
    end
endmodule
