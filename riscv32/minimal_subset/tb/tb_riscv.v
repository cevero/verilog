module tb_riscv();
  reg clk;

  mips processor(clk);

  always #1 clk = !clk;
  initial begin
    $dumpfile("build/wave.vcd");
    $dumpvars(0, tb_riscv);
    clk = 1'b0;
    #200 $finish;
  end
endmodule
