module datapath(
  input clk,
  input reg_dst,
  input branch,
	input mem_read,
  input mem_to_reg,
  input mem_write,
  input alu_src,
  input reg_write,
  input  [3:0] alu_ctrl,
  output [5:0] func_code,
  output [5:0] opcode
);

	wire zero;
	wire mux_add;
	wire [4:0] write_register;
  wire [31:0] pc_i;
  wire [31:0] pc_o;
  wire [31:0] instruction;
	wire [31:0] add_pc;
	wire [31:0] read_data1;
	wire [31:0] read_data2;
	wire [31:0] sigextend_o;
	wire [31:0] shift_left2;
	wire [31:0] add_result;
	wire [31:0] mux_alu;
	wire [31:0] alu_result;
	wire [31:0] read_data;
	wire [31:0] write_data;

  // mux
  assign write_register = reg_dst ? instruction[15:11] : instruction[20:16];
  assign mux_alu = alu_src ? sigextend_o : read_data2;
  assign pc_i = mux_add ? add_pc : add_result;
	assign write_data = mem_to_reg ? read_data : alu_result;

	// shift left 2
  assign shift_left2 = (sigextend_o << 2);

  // opcode assign
  assign opcode = instruction[31:26];

  // func_code assign
  assign func_code = instruction[5:0];

  // adders
  assign add_pc = pc_o + 32'd4;
  assign add_result = add_pc + shift_left2;

	// isntatiations of the components
  register PC(
    .clk(clk),
    .en(1'b1),
    .in(pc_i),
    .out(pc_o)
  );

  instruction_memory INSTRUCTION_MEMORY(
    .addr(pc_o),
    .instruction(instruction)
  );  

  register_file REGISTER_FILE(
    .clk(clk),
    .reg_write(reg_write),
    .read_register1(instruction[25:21]),
    .read_register2(instruction[20:16]),
    .write_register(write_register),
    .write_data(write_data),
    .read_data1(read_data1),
    .read_data2(read_data2)
  );

  sigextend SIGEXTEND(
    .in(instruction[15:0]),
    .out(sigextend_o)
  );

	alu ALU(
		.alu_ctrl(alu_ctrl),
		.in_a(read_data1),
		.in_b(mux_alu),
		.zero(zero),
		.out(alu_result)
	);

	memory
  #(
    .ADR(32),
    .WIDTH(32),
    .DPTH(10)
  )
  DM
	(
		.mem_write(mem_write),
		.mem_read(mem_read),
		.write(read_data2),
		.addr(alu_result),
		.read(write_data)
	);
endmodule
