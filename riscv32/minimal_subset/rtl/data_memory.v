module memory
#(
	parameter ADR   = 32,
	parameter WIDTH = 32,
	parameter DPTH  = 4294967296
)
(
	input mem_write, 
	input mem_read,
	input  [WIDTH-1:0] write,
	input  [ADR-1:0] addr,
	output reg [WIDTH-1:0] read
);

	reg [WIDTH-1:0] mem [DPTH-1:0];

	always @ (*)
    //mem[0] = 32'd2;
    //mem[1] = 32'd3;
		if (mem_write == 1'b1 && mem_read == 1'b0) 
			mem [addr] = write;
		else if(mem_write == 1'b0 && mem_read == 1'b1)
			read = mem [addr];
endmodule
