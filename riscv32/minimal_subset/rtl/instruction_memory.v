module instruction_memory
#(
	parameter ADR   = 32,
	parameter WIDTH = 32
)
(
	input  [ADR-1:0] addr,
	output reg [WIDTH-1:0] instruction
);

  // program
	always @ (*) 
    case(addr)
      32'd0: instruction <= 32'd00000000000000000000000000000000;
      32'd1: instruction <= 32'd00000000000000000000000000000000;
      32'd2: instruction <= 32'd00000000000000000000000000000000;
      32'd3: instruction <= 32'd00000000000000000000000000000000;
    endcase	
endmodule
