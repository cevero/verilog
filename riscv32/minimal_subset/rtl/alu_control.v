module alu_control(
	input [1:0] alu_op,
	input [5:0] func_code,
	output reg [3:0] alu_ctrl
);

	always @(*)
    case(alu_op)	
      2'b00: alu_ctrl <= 0010;
      2'b01: alu_ctrl <= 0110;
      2'b10:
        case(func_code)
          6'd32: alu_ctrl <= 4'b0010;	
          6'd34: alu_ctrl <= 4'b0110;	
          6'd36: alu_ctrl <= 4'b0000;	
          6'd37: alu_ctrl <= 4'b0001;	
          6'd39: alu_ctrl <= 4'b1100;	
          6'd42: alu_ctrl <= 4'b0111;	
        endcase			
    endcase
endmodule	
