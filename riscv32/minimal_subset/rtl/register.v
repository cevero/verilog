module register
#(
  parameter WIDTH = 32
)
(
  input clk,
  input en,
  input [WIDTH-1:0] in,
  output reg [WIDTH-1:0] out
);

  always @ (posedge clk)
    if(en)
      out <= in;
endmodule      
