module tb_pc;
  reg [12:0] data;
  reg clk;
  wire [12:0] Q;
  wire [12:0] Qbar;

  pc uut(
    .data(data),
    .clk(clk),
    .Q(Q),
    .Qbar(Qbar)
  );

  always #5 clk = ~clk;

  initial begin
    $dumpfile("build/wave.vcd");
    $dumpvars(0, tb_pc);

    clk = 0;

    #10 data = 12'd1;
    #10 data = 12'd6;
    #10 data = 12'd8;
    #10 data = 12'd50;
    #10 $finish;
  end
endmodule
