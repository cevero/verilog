module pc(
	input [12:0] data,
	input clk,
	output [12:0] Q,
	output [12:0] Qbar
        );

	d U0(data[0],clk,Q[0],Qbar[0]);
	d U1(data[1],clk,Q[1],Qbar[1]);
	d U2(data[2],clk,Q[2],Qbar[2]);
	d U3(data[3],clk,Q[3],Qbar[3]);
	d U4(data[4],clk,Q[4],Qbar[4]);
	d U5(data[5],clk,Q[5],Qbar[5]);
	d U6(data[6],clk,Q[6],Qbar[6]);
	d U7(data[7],clk,Q[7],Qbar[7]);
	d U8(data[8],clk,Q[8],Qbar[8]);
	d U9(data[9],clk,Q[9],Qbar[9]);
	d U10(data[10],clk,Q[10],Qbar[10]);
	d U11(data[11],clk,Q[11],Qbar[11]);
	d U12(data[12],clk,Q[12],Qbar[12]);
endmodule
