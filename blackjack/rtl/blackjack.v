module blackjack(
  input clk,
  input reset,
  input hit,
  input stay,
  input [3:0] card,
  output reg get_card,
  output reg p_round,
  output reg d_round,
  output reg draw,
  output reg p_win,
  output reg d_win,
  output reg [5:0] d_sum,
  output reg [5:0] p_sum
);

  localparam START = 4'd0;
  localparam SETUP_D_CARD = 4'd1;
  localparam SETUP_D_SUM_CARD = 4'd2;
  localparam SETUP_P_CARD = 4'd3;
  localparam SETUP_P_SUM_CARD = 4'd4;
  localparam TEST = 4'd5;
  localparam ASK_P = 4'd6;
  localparam P_GET_CARD = 4'd7;
  localparam D_GET_CARD = 4'd8;
  localparam D_TEST = 4'd9;
  localparam FINISH = 4'd10;

  reg [3:0] state;

  always @(posedge clk or posedge reset) begin
    if(reset)
      state = START;
    else
      case (state)
        START:
          begin
            d_sum <= 6'd0;
            p_sum <= 6'd0;
            state <= SETUP_D_CARD;
          end
        SETUP_D_CARD:
          begin
            d_sum <= d_sum + card;
            state <= SETUP_D_SUM_CARD;
          end
        SETUP_D_SUM_CARD: 
            state <= SETUP_P_CARD;
        SETUP_P_CARD:
          state <= SETUP_P_SUM_CARD;
        SETUP_P_SUM_CARD:
          begin
            p_sum <= p_sum + card;
            state <= TEST;
          end
        TEST: 
          begin
            p_sum = p_sum + card;
            if (p_sum >= 21)
              state <= FINISH;
            else
              state <= ASK_P;
          end
        ASK_P:
          if (hit)
            state <= P_GET_CARD;
          else if (stay)
            state <= D_GET_CARD;
        P_GET_CARD:
          state <= TEST;
        D_GET_CARD:
          state <= D_TEST;
        D_TEST:
          begin
            d_sum = d_sum + card;
            if (d_sum >= 17)
              state <= FINISH;
            else
              state <= D_GET_CARD;
          end
        FINISH:
          state <= FINISH;
        default:
          state <= START;
      endcase
  end

  always @(state) begin
    case (state) 
      START:
        begin
          get_card = 1'b0;
          p_round = 1'b0;
          d_round = 1'b0;
          draw = 1'b0;
          p_win = 1'b0;
          d_win = 1'b0;
        end
      SETUP_D_CARD:
        get_card = 1'b1;
      SETUP_D_SUM_CARD: 
        begin
          get_card = 1'b0;
        end
      SETUP_P_CARD:
        get_card = 1'b1;
      SETUP_P_SUM_CARD:
        begin
          get_card = 1'b1;
        end
      TEST: 
        begin
          get_card = 1'b0;
        end
      ASK_P:
        p_round = 1'b1;
      P_GET_CARD:
        begin
          p_round = 1'b0;
          get_card = 1'b1;
        end
      D_GET_CARD:
        begin
          d_round = 1'b1;
          p_round = 1'b0;
          get_card = 1'b1;
        end
      D_TEST:
        begin
          get_card = 1'b1;
        end
      FINISH:
        begin
          get_card = 1'b0;
          p_round = 1'b0;
          d_round = 1'b0;
          if (d_sum == p_sum)
            draw = 1'b1;
          else if (d_sum == 6'd21 || p_sum > 6'd21 || (d_sum > p_sum && d_sum <= 6'd21))
            d_win = 1'b1;
          else if (p_sum == 6'd21  || d_sum > 6'd21 || (p_sum > d_sum && p_sum <= 6'd21))
            p_win = 1'b1;
          else
            draw = 1'b1;
        end
      default:
        begin
          get_card = 1'b0;
          p_round = 1'b0;
          d_round = 1'b0;
          draw = 1'b0;
          p_win = 1'b0;
          d_win = 1'b0;
        end
    endcase
  end
endmodule
