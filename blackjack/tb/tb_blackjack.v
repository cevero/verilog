module tb_blackjack();
  reg clk;
  reg reset;
  reg hit;
  reg stay;
  reg [3:0] card;
  wire get_card;
  wire p_round;
  wire d_round;
  wire draw;
  wire p_win;
  wire d_win;
  wire [5:0] d_sum;
  wire [5:0] p_sum;

  blackjack dut(
          .clk(clk),
          .reset(reset),
          .hit(hit),
          .stay(stay),
          .card(card),
          .get_card(get_card),
          .p_round(p_round),
          .d_round(d_round),
          .draw(draw),
          .p_win(p_win),
          .d_win(d_win),
          .d_sum(d_sum),
          .p_sum(p_sum)
  );

  always #5 clk = !clk;

  initial begin
    $dumpfile("build/wave.vcd");
    $dumpvars(0, tb_blackjack);
    clk = 1'b0;
    reset = 1'b1;
    card = 4'd0;
    hit = 1'b0;
    stay = 1'b0;
    #10 reset = 1'b0;
    card = 4'd4;
    #10 card = 4'd10;
    #20 card = 4'd7;
    #40 hit = 1'b1;
    #10 hit = 1'b0;
    #40 reset = 1'b1;
    #10 reset = 1'b0;
    #5 card = 4'd3;
    #20 card = 4'd7;
    #20 card = 4'd10;
    #20 card = 4'd4;
    #10 stay = 1'b1;
    #10 stay = 1'b0;
    #60 card = 4'd5;
    #40 reset = 1'b1;
    #10 reset = 1'b0;
    #5 card = 4'd3;
    #20 card = 4'd7;
    #20 card = 4'd10;
    #20 card = 4'd4;
    #10 stay = 1'b1;
    #10 stay = 1'b0;
    #60 card = 4'd6;
    #40 $finish;
  end
endmodule
