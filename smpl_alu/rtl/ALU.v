/* ALU Arithmetic and Logic Operations
----------------------------------------------------------------------
|ALU_Sel|   ALU Operation
----------------------------------------------------------------------
| 00  |   ALU_Out = A + B;
----------------------------------------------------------------------
| 01  |   ALU_Out = A - B;
----------------------------------------------------------------------
| 10  |   ALU_Out = A and B;
----------------------------------------------------------------------
| 11  |   ALU_Out = A not B;
----------------------------------------------------------------------*/

module alu(
	input [7:0] A,B,
	input [1:0] ALU_Sel,
	output [7:0] ALU_Out
	);

	reg [7:0] ALU_Result;
  assign ALU_Out = ALU_Result; // ALU out

  always @ (*) begin
    case(ALU_Sel)
	    4'b00: // Addition
				ALU_Result = A + B ; 
			4'b01: // Subtraction
			  ALU_Result = A - B ;
			4'b10: // Multiplication
			  ALU_Result = A & B;
			4'b11: // Division
			  ALU_Result = ~A;
			default: ALU_Result = A + B; 
    endcase
  end
endmodule
