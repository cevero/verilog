module tb_alu;
//Inputs
 reg[7:0] A,B;
 reg[1:0] ALU_Sel;

//Outputs
 wire[7:0] ALU_Out;
 // Verilog code for ALU
 integer i;

 alu dut(
            A,B,  // ALU 8-bit Inputs                 
            ALU_Sel,// ALU Selection
            ALU_Out // ALU 8-bit Output
     );

  initial begin
		$dumpfile("build/wave.vcd");
		$dumpvars(0, tb_alu);
		$display("time\t A\t B\t ALU_Sel\t ALU_Out\t");
		$monitor("%g\t %b\t %b\t %b\t\t %b\t\t", $time, A, B, ALU_Sel, ALU_Out);
  	A = 8'h0A;
  	B = 8'h02;
  	ALU_Sel = 2'h0;
  
  	for (i=0;i<=3;i=i+1) 
   		#10 ALU_Sel = ALU_Sel + 2'h1;
			
  	A = 8'hF6;
  	B = 8'h0A;
  end
endmodule
