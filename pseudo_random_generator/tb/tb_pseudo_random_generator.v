module tb_pseudo_random_generator;
  reg clk;
  reg reset; 
  reg en; 
  wire [5:0] q;

  pseudo_random_generator dut(
    .clk(clk),
    .reset(reset),
    .en(en),
    .q(q)
  );

  initial begin
    $dumpfile("build/wave.vcd");
    $dumpvars(0, tb_pseudo_random_generator);
    clk = 1'b0;
    reset = 1'b1;
    en = 1'b0;
    #10 reset = 1'b0;
    en = 1'b1;
    #1000 $finish;
  end

  always #1 clk = ~clk;

endmodule
