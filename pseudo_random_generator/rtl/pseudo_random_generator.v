module pseudo_random_generator(
  input clk,
  input reset, 
  input en, 
  output reg [5:0] q
);

  always @(posedge clk or posedge reset) begin
    if (reset)
      q <= 6'd1; // can be anything except zero
    else if (en)
      //q <= {q[6:0], q[7] ^ q[5] ^ q[4] ^ q[3]}; // polynomial for maximal LFSR
      q <= {q[4:0], q[5] ^ q[4]}; // polynomial for maximal LFSR
  end
endmodule
