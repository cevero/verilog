module tb_card_generator;
  reg clk;
  reg reset;
  reg get_card;
  wire [3:0] card;

  card_generator uut(
    .clk(clk),
    .reset(reset),
    .get_card(get_card),
    .card(card)
  );

  initial begin
    $dumpfile("build/wave.vcd");
    $dumpvars(0, tb_card_generator);
    clk = 1'b0;
    reset = 1'b1;
    get_card = 1'b0;
    #10 reset = 1'b0;
    #30 get_card = 1'b1;
    #10000 $finish;
  end

  always #5 clk = ~clk;
endmodule
