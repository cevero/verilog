module card_generator(
  input clk,
  input reset,
  input get_card,
  output reg [3:0] card
);

  localparam START = 3'd0;
  localparam GET = 3'd1;
  localparam VALID = 3'd2;
  localparam NEXT = 3'd3;
  localparam OUT = 3'd4;

  reg [2:0] state;
  reg [3:0] int_card;
  reg [5:0] card_addr;
  reg [51:0] used_card;

  always @(card_addr) begin
    case(card_addr)
      6'd0: int_card <= 4'd5;
      6'd1: int_card <= 4'd5;
      6'd2: int_card <= 4'd3;
      6'd3: int_card <= 4'd8;
      6'd4: int_card <= 4'd6;
      6'd5: int_card <= 4'd9;
      6'd6: int_card <= 4'd2;
      6'd7: int_card <= 4'd3;
      6'd8: int_card <= 4'd6;
      6'd9: int_card <= 4'd7;
      6'd10: int_card <= 4'd3;
      6'd11: int_card <= 4'd7;
      6'd12: int_card <= 4'd10;
      6'd13: int_card <= 4'd9;
      6'd14: int_card <= 4'd11;
      6'd15: int_card <= 4'd11;
      6'd16: int_card <= 4'd10;
      6'd17: int_card <= 4'd9;
      6'd18: int_card <= 4'd4;
      6'd19: int_card <= 4'd10;
      6'd20: int_card <= 4'd10;
      6'd21: int_card <= 4'd10;
      6'd22: int_card <= 4'd5;
      6'd23: int_card <= 4'd10;
      6'd24: int_card <= 4'd8;
      6'd25: int_card <= 4'd10;
      6'd26: int_card <= 4'd10;
      6'd27: int_card <= 4'd11;
      6'd28: int_card <= 4'd6;
      6'd29: int_card <= 4'd2;
      6'd30: int_card <= 4'd4;
      6'd31: int_card <= 4'd11;
      6'd32: int_card <= 4'd10;
      6'd33: int_card <= 4'd10;
      6'd34: int_card <= 4'd9;
      6'd35: int_card <= 4'd2;
      6'd36: int_card <= 4'd5;
      6'd37: int_card <= 4'd4;
      6'd38: int_card <= 4'd6;
      6'd39: int_card <= 4'd10;
      6'd40: int_card <= 4'd7;
      6'd41: int_card <= 4'd8;
      6'd42: int_card <= 4'd10;
      6'd43: int_card <= 4'd7;
      6'd44: int_card <= 4'd10;
      6'd45: int_card <= 4'd2;
      6'd46: int_card <= 4'd10;
      6'd47: int_card <= 4'd3;
      6'd48: int_card <= 4'd4;
      6'd49: int_card <= 4'd10;
      6'd50: int_card <= 4'd8;
      6'd51: int_card <= 4'd10;
      default: int_card <= 4'd0;
    endcase
  end

  always @(posedge clk or posedge reset) begin
    if(reset) begin
      used_card = 52'd255;
      card_addr = 6'd0;
      state = START;
    end
    else begin
      if(card_addr == 6'd51)
        card_addr = 6'd0;
      else
        card_addr = card_addr + 1;
      if(used_card == 52'hFFFFFFFFFFFFF)
        used_card = 52'd0;
      case(state)
        START:
          if(get_card)
            state = GET;
        GET:
          if(used_card[card_addr] || (int_card == 4'd0))
            state = GET;
          else
            state = OUT;
        OUT:
          state = START;
        default:
          state = START;
      endcase
    end
  end

  always @(state) begin
    case(state)
      OUT:
        begin
          used_card[card_addr] = 1'b1;
          card = int_card;
        end
    endcase
  end
endmodule

