module tb_digit();
  reg  clk, rst, init, mod_in;
  wire mod_out;
  wire [3:0] q;

  digit dut(
      .clk(clk),
      .rst(rst),
      .init(init),
      .mod_in(mod_in),
      .mod_out(mod_out),
      .q(q)
  );

  initial
    begin
      clk = 0;
      rst = 1;
      mod_in = 0;
      init = 0;
      forever #1 clk = ~clk;
    end

  initial
    begin
      $dumpfile("build/wave.vcd");
      $dumpvars(0, tb_digit);
      #5 rst = 0;
      #5 init = 1;
      #2 mod_in = 1;
      #7 init = 0;

      #20 $finish;
    end
endmodule	
