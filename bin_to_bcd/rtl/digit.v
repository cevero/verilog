module digit(
    input clk,
    input rst,
    input init,
    input mod_in,
    output reg mod_out,
    output reg [3:0] q
);

    reg [3:0] q_int;
    reg [2:0] next_q_int;
    reg next_mod_out;

    always @(q_int) begin
        case (q_int)
            4'd0: next_q_int = 3'b000;
            4'd1: next_q_int = 3'b001;
            4'd2: next_q_int = 3'b010;
            4'd3: next_q_int = 3'b011;
            4'd4: next_q_int = 3'b100;
            4'd5: next_q_int = 3'b000;
            4'd6: next_q_int = 3'b001;
            4'd7: next_q_int = 3'b010;
            4'd8: next_q_int = 3'b011;
            4'd9: next_q_int = 3'b100;
            default: next_q_int = 3'b100;
        endcase

        if (q_int >= 5)
            next_mod_out = 1'b1; 
        else
            next_mod_out = 1'b0;
    end

    always @(*) begin
        mod_out = (next_mod_out && !init);
        q = q_int;
    end

    always @(posedge clk, rst) begin
        if (rst)
            q_int = 4'd0; 
        else
            if (init) begin
                q_int = 4'd0;
                q_int[0] = mod_in; // set LSB initialization
            end
            else
                q_int = {next_q_int, mod_in};
    end
endmodule
