module bin_to_bcd(
    input clk,
    input rst,
    input init,
    input mod_in,
    output mod_out,
    output [(4*DIGITS-1):0] q
);

    parameter DIGITS = 1;

