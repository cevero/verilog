module cd4017(
  input clk,
  input clk_inhibit,
  input reset,
  output reg [9:0] out,
  output reg carry_out
);

  always @(posedge clk, posedge clk_inhibit, posedge reset) begin
    if (reset || (out > 10'h100))
      out = 10'h1;
    else if(!clk_inhibit)
      out =  out << 1;

    if (out >= 10'h20)
      carry_out = 1'b0;
    else
      carry_out = 1'b1;
  end
endmodule

