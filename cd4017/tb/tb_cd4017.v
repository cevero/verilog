module tb_cd4017;
  reg clk, clk_inhibit, reset;
  wire carry_out;
  wire [9:0] out; 

  cd4017 uut(
    clk,
    clk_inhibit,
    reset,
    out,
    carry_out
  );

  initial begin
    clk = 1'b0;
    clk_inhibit = 1'b0;
    reset = 1'b1;
    forever #5 clk = ~clk;
  end

  initial begin
    $dumpfile("build/wave.vcd");
    $dumpvars(0, tb_cd4017);
    //$display("time\t in\t out");
    //$monitor("%g\t %h\t %b", $time, in, out);
    #10 reset = 1'b0;
    #30 clk_inhibit = 1'b1;
    #30 clk_inhibit = 1'b0;
    #30 reset = 1'b1;
    #5 reset = 1'b0;
    #200 $finish;
  end

endmodule
    

