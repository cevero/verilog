module tb_sum_pc;
  reg [12:0] data_i;
  wire [12:0] data_o;

  sum_pc uut(
    .data_i(data_i),
    .data_o(data_o)
  );

  initial begin
    $dumpfile("build/wave.vcd");
    $dumpvars(0, tb_sum_pc);

    #10 data_i = 12'd1;
    #10 data_i = 12'd6;
    #10 data_i = 12'd8;
    #10 data_i = 12'd50;
    #10 $finish;
  end
endmodule
