module tb_latch();
	reg R,S;

	latch dut(R,S,Q,Qbar);

	initial begin
		$dumpfile("build/wave.vcd");
		$dumpvars(0, tb_latch);
		$display("time\t R\t S\t Q\t Qbar\t");
		$monitor("%g\t %b\t %b\t %b\t %b", $time, R, S, Q, Qbar);
		R <= 0; S <= 0;
		#5 R <= 1;
		#5 R <= 0; S <= 1;
		#5 R <= 1;
		#5 $finish;
	end
endmodule	
