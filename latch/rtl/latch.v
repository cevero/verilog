module latch(R,S,Q,Qbar);
	input R,S;
	output Q,Qbar;
	nor U0(Q,R,Qbar);
	nor U1(Qbar,S,Q);
endmodule	
