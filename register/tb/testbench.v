module tb_register();
	wire [7:0] Q;
	reg [7:0] data;
	reg clk;

	register dut(data,clk,Q);

	always #5 clk = ~clk;
	initial begin 
		$dumpfile("build/wave.vcd");
		$dumpvars(0, tb_register);
		$display("time\t clk\t data\t\t Q");
		$monitor("%g\t %b\t %b\t %b\t", $time, clk, data, Q);
		data <= 8'b01010101;
		clk <= 0;
		#10 data <= 8'b10101010;
		#10 $finish;
	end
endmodule
