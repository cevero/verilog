module jk(J,K,clk,Q,Qbar);
	input J,K,clk;
	output Q,Qbar;

	wire w_j,w_k;

	nand U0(w_j,Qbar,J,clk);
	nand U1(w_k,Q,K,clk);
	nand U2(Q,w_j,Qbar);
	nand U3(Qbar,Q,w_k);
endmodule
