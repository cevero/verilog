module tb_jk();
	reg J,K,clk;

	jk dut(J,K,clk,Q,Qbar);
	
	always #5 clk = ~clk;
	initial begin 
		$dumpfile("build/wave.vcd");
		$dumpvars(0, tb_jk);
		$display("time\t clk\t J\t K\t Q\t Qbar");
		$monitor("%g\t %b\t %b\t %b\t %b\t %b", $time, clk, J, K, Q, Qbar);
		J <= 0; clk <= 0; K <= 0; 
		#10 J <= 1;
		#10 J <= 0; K <= 1;
		#10 J <= 1;
		#10 $finish;
	end
endmodule
