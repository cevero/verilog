module sr(S,R,clk,Q,Qbar);
	input S,R,clk;
	output Q,Qbar;

	//internal wire connections
	wire w_s,w_r;	

	nand U0(w_s,S,clk);
	nand U1(w_r,clk,R);
	nand U2(Q,w_s,Qbar);
	nand U3(Qbar,Q,w_r);
endmodule
