module tb_sr();
	reg S,R,clk;

	sr dut(S,R,clk,Q,Qbar);

	always #5 clk = ~clk;
	initial begin 
		$dumpfile("build/wave.vcd");
		$dumpvars(0, tb_sr);
		$display("time\t clk\t S\t R\t Q\t Qbar");
		$monitor("%g\t %b\t %b\t %b\t %b\t %b", $time, clk, S, R, Q, Qbar);
		S <= 0; clk <= 0; R <= 0; 
		#10 S <= 1;
		#10 S <= 0; R <= 1;
		#10 S <= 1;
		#10 R <= 0;
		#10 S <= 0;
		#10 $finish;
	end
endmodule
