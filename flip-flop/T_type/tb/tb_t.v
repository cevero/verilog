module tb_t();
	reg T,clk;

	t dut(T,clk,Q,Qbar);
	
	always #1 clk = !clk;
	initial begin 
		$dumpfile("build/wave.vcd");
		$dumpvars(0, tb_t);
		$display("time\t clk\t T\t Q\t Qbar");
		$monitor("%g\t %b\t %b\t %b\t %b", $time, clk, T, Q, Qbar);
		T <= 0; clk <= 0; 
		#2 T <= 1;
		#2 $finish;
	end
endmodule
