module t(T,clk,Q,Qbar);
	input T,clk;
	output Q,Qbar;

	wire w_U02,w_U13;

	and U0(w_U02,T,Q,clk);
	and U1(w_U13,T,Qbar,clk);
	nor U2(Q,w_U02,Qbar);
	nor U3(Qbar,Q,w_U13);
endmodule
