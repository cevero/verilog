module tb_ffd();
	reg D, clk;

	d dut(D,clk,Q,Qbar);

	always #5 clk = ~clk;
	initial begin 
		$dumpfile("build/wave.vcd");
		$dumpvars(0, tb_ffd);
		$display("time\t clk\t D\t Q\t Qbar");
		$monitor("%g\t %b\t %b\t %b\t", $time, clk, D, Q, Qbar);
		D <= 0;
		clk <= 0;
		#10 D <= 1;
		#10 D <= 0;
		#10 $finish;
	end
endmodule
