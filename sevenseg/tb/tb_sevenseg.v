module tb_sevenseg();
  reg [3:0] in;
  wire [6:0] out;

  sevenseg sseg(in, out);

  initial begin
    $dumpfile("build/wave.vcd");
    $dumpvars(0, tb_sevenseg);
    $display("time\t in\t out");
    $monitor("%g\t %h\t %b", $time, in, out);
    #5 in = 4'h0;
    #5 in = 4'h1;
    #5 in = 4'h2;
    #5 in = 4'h3;
    #5 in = 4'h4;
    #5 in = 4'h5;
    #5 in = 4'h6;
    #5 in = 4'h7;
    #5 in = 4'h8;
    #5 in = 4'h9;
    #5 in = 4'ha;
    #5 in = 4'hb;
    #5 in = 4'hc;
    #5 in = 4'hd;
    #5 in = 4'he;
    #5 in = 4'hf;
    #5 $finish;
  end
endmodule	
