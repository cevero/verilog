module sevenseg(
  input  wire [3:0] data,
  output wire [6:0] out     
);

  reg [6:0] hex;

always @(*)
begin
  case(data[3:0])
    4'h0: hex = 7'b0111111;
    4'h1: hex = 7'b0000110;
    4'h2: hex = 7'b1011011;
    4'h3: hex = 7'b1001111;
    4'h4: hex = 7'b1100110;
    4'h5: hex = 7'b1101101;
    4'h6: hex = 7'b1111101;
    4'h7: hex = 7'b0000111;
    4'h8: hex = 7'b1111111;
    4'h9: hex = 7'b1101111;
    4'ha: hex = 7'b1110111;
    4'hb: hex = 7'b1111100;
    4'hc: hex = 7'b0111001;
    4'hd: hex = 7'b1011110;
    4'he: hex = 7'b1111001;
    4'hf: hex = 7'b1110001;
    default: hex = 7'b1011100; // o
  endcase
end

assign out = ~hex; //anodo comum

endmodule
