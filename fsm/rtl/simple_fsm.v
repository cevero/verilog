module fsm(
	input [3:0] value,
	input clk,
	input reset,
	output reg final,
	output reg sig,
	output reg [3:0] sum
);	
	
	localparam s0 = 3'd0;
	localparam s1 = 3'd1;
	localparam s2 = 3'd2;
	localparam s3 = 3'd3;
	localparam s4 = 3'd4;
	localparam s5 = 3'd5;

	reg [2:0] state = s0;

	always @ (posedge clk or posedge reset) begin
		if(reset)
			state = s0;
		else
			case(state)
				s0:
                                  begin
                                    state = s1;
                                    sum <= 4'd0;
                                  end
				s1: state = s2;
				s2:
                                  begin
                                    state = s3;
                                    sum <= sum + value;
                                  end
				s3: state = s4;
				s4: 
                                  begin
                                    state = s5;
                                    sum <= sum + value;
                                  end
				s5: state = s5;
				default: state = s0;
			endcase
	end	

	always @(state) begin
		case(state)
			s0: begin
				sig <= 1'b0;
				final <= 1'b0;
			end
			s1: sig <= 1'b1;
			s2: sig <= 1'b0;
			s3: sig <= 1'b1;
			s4: sig <= 1'b0;
			s5: final =1'b1;
		endcase
	end				
endmodule	
