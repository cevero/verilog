module tb_fsm();
	reg [3:0] value;
	reg clk,reset;
	wire [3:0] sum;

	fsm dut(value,clk,reset,final,sig,sum);

	always #2 clk = !clk;
	initial begin
		$dumpfile("build/wave.vcd");
		$dumpvars(0, tb_fsm);
		value <= 4'd4; clk <= 1'b0; reset <= 1'b1;
		#10 reset <= 1'b0;
		#50 $finish;
	end	
endmodule
