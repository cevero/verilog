module mux(in0, in1, sel, out);
	input in0, in1, sel;
	output out;
	reg out;
	always @ (sel or in0 or in1)
		out = (sel) ? in1 : in0;
endmodule
