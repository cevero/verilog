module tb_pipeline();
	reg clk, in1;
	reg [7:0] in2;
	wire out;
	wire [7:0] out2;

	pipeline dut(clk, in1, in2, out, out2);

	always #1 clk = !clk;
	initial begin
		$dumpfile("build/wave.vcd");
		$dumpvars(0, tb_pipeline);
		clk <= 0; in1 <= 0; in2 <= 8'd0;
		#2 in1 <= 1; in2 <= 8'd10;
		#2 in1 <= 0; in2 <= 8'd20;
		#2 in1 <= 1; in2 <= 8'd30;
		$finish;
	end
endmodule	
