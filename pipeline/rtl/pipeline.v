// a extremely simple example of a pipeline

module pipeline(
	input clk,
	input in1,
	input [7:0] in2,
	output reg out1,
	output reg [7:0] out2
);

	always @ (posedge clk) begin 
		out1 <= in1;
		out2 <= in2;
	end		
endmodule			
