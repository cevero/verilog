module sram(dataIn, dataOut, Addr, cs, we, rd, Clk);
    parameter ADR   = 4;
    parameter DAT   = 8;
    parameter DPTH  = 16;

    input   [DAT-1:0]  dataIn;
    output reg [DAT-1:0]  dataOut;
    input   [ADR-1:0]  Addr;
    input cs, we, rd, Clk;

    reg [DAT-1:0] SRAM [DPTH-1:0];

    always @ (posedge Clk)
    begin
        if (cs == 1'b1) begin
            if (we == 1'b1 && rd == 1'b0) begin
                SRAM [Addr] = dataIn;
            end
            else if (rd == 1'b1 && we == 1'b0) begin
                dataOut = SRAM [Addr]; 
            end
        end
    end
endmodule
