module tb_pc_complete;
  reg clk;
  reg sel;
  wire [12:0] out;

  pc_complete uut(
    .clk(clk),
    .sel(sel),
    .out(out)
  );

  always #5 clk = ~clk;

  initial begin
    $dumpfile("build/wave.vcd");
    $dumpvars(0, tb_pc_complete);

    clk = 1'b0;
    sel = 1'b0;
    
    #20 sel = 1'b1;

    #100 $finish;
  end
endmodule
