module d(D,clk,Q,Qbar);
	input D,clk;
	output Q,Qbar;

	wire w_Dbar,w_U13;

	nand U0(w_Dbar,D,clk);
	nand U1(w_U13,w_Dbar,clk);
	nand U2(Q,w_Dbar,Qbar);
	nand U3(Qbar,Q,w_U13);
endmodule
