module pc_complete(
  input clk,
  input sel,
  output [12:0] out
);

  wire [12:0] data_o, pc_in;

  assign pc_in = (sel) ? data_o : {13{1'b0}};
  //assign pc_in = {13{1'b0}};

  sum_pc spc(
    .data_i(out),
    .data_o(data_o)
  );

  pc pc1(
    .data(pc_in),
    .clk(clk),
    .Q(out)
  );
endmodule
 
