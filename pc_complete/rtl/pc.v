module pc(
  input [12:0] data,
  input clk,
  output reg [12:0] Q,
  output [12:0] Qbar
);

  always @(posedge clk)
    Q = data;
endmodule
