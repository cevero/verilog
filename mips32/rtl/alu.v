module alu(
  input [3:0] alu_ctrl,
  input [31:0] in_a,
  input [31:0] in_b,
  output zero,
  output reg [31:0] out
);

localparam ADD = 4'b0010;
localparam SUB = 4'b0110;
localparam AND = 4'b0000;
localparam OR  = 4'b0001;
localparam SLT = 4'b0111;
localparam NOR = 4'b1100;


	assign zero = (out == 32'd0);

  always @ (alu_ctrl, in_a, in_b)
    case(alu_ctrl)
      ADD: out <= in_a + in_b;
      SUB: out <= in_a - in_b;
      AND: out <= in_a & in_b;
      OR:  out <= in_a | in_b;
      SLT: out <= (in_a < in_b) ? 32'd1 : 32'd0;
			NOR: out <= ~(in_a | in_b);
    endcase
endmodule    
