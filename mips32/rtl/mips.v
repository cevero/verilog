module mips(
  input clk
);

  wire reg_dst;
  wire branch;
	wire mem_read;
  wire mem_to_reg;
  wire mem_write;
  wire alu_src;
  wire reg_write;
  wire [1:0] alu_op;
  wire [3:0] alu_ctrl;
  wire [5:0] func_code;
	wire [5:0] opcode;

  datapath DATAPATH(
    .clk(clk),
    .reg_dst(reg_dst),
    .branch(branch),
    .mem_read(mem_read),
    .mem_to_reg(mem_to_reg),
    .mem_write(mem_write),
    .alu_src(alu_src),
    .reg_write(reg_write),
    .alu_ctrl(alu_ctrl),
    .func_code(func_code),
    .opcode(opcode)
  );

	alu_control ALU_CONTROL(
		.alu_op(alu_op),
		.func_code(func_code),
		.alu_ctrl(alu_ctrl)
	);


  controller CONTROLLER(
    .clk(clk),
    .opcode(opcode),
    .reg_dst(reg_dst),
    .branch(branch),
    .mem_read(mem_read),
    .mem_to_reg(mem_to_reg),
    .mem_write(mem_write),
    .alu_src(alu_src),
    .reg_write(reg_write),
    .alu_op(alu_op)
  );
endmodule
