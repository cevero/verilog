module controller(
	input clk,
	input [5:0] opcode,
  output reg reg_dst,
  output reg branch,
	output reg mem_read,
  output reg mem_to_reg,
  output reg mem_write,
  output reg alu_src,
  output reg reg_write,
  output reg [1:0] alu_op
);

localparam R_TYPE = 6'b000000;
localparam LW     = 6'b100011;
localparam SW     = 6'b101011;
localparam BEQ    = 6'b000100;

	always @ (*)
		case(opcode)
      R_TYPE: begin
        reg_dst    <= 1'b1;
        branch     <= 1'b0;
        mem_read   <= 1'b0;
        mem_to_reg <= 1'b0;
        mem_write  <= 1'b0;
        alu_src    <= 1'b0;
        reg_write  <= 1'b1;
        alu_op     <= 2'b10;
      end
      LW: begin
        reg_dst    <= 1'b0;
        branch     <= 1'b0;
        mem_read   <= 1'b1;
        mem_to_reg <= 1'b1;
        mem_write  <= 1'b0;
        alu_src    <= 1'b1;
        reg_write  <= 1'b1;
        alu_op     <= 2'b00;
      end
      SW: begin
        reg_dst    <= 1'bx;
        branch     <= 1'b0;
        mem_read   <= 1'b0;
        mem_to_reg <= 1'bx;
        mem_write  <= 1'b1;
        alu_src    <= 1'b1;
        reg_write  <= 1'b0;
        alu_op     <= 2'b00;
      end
      BEQ: begin
        reg_dst    <= 1'bx;
        branch     <= 1'b1;
        mem_read   <= 1'b0;
        mem_to_reg <= 1'bx;
        mem_write  <= 1'b0;
        alu_src    <= 1'b0;
        reg_write  <= 1'b0;
        alu_op     <= 2'b01;
      end
		endcase
endmodule
