module register_file(
  input clk,
  input reg_write,
  input [4:0] read_register1,
  input [4:0] read_register2,
  input [4:0] write_register,
  input [31:0] write_data,
  output reg [31:0] read_data1,
  output reg [31:0] read_data2
);

  reg [31:0] registers [31:0];  // 32 register of 32 bits each

  always @ (posedge clk)
    if(reg_write)
      registers[write_register] <= write_data;
  
  always @ (negedge clk) begin 
    read_data1 <= read_register1;
    read_data2 <= read_register2;
  end  
endmodule    
