module tb_mips();
  reg clk;

  mips processor(clk);

  always #1 clk = !clk;
  initial begin
    $dumpfile("build/wave.vcd");
    $dumpvars(0, tb_mips);
    clk = 1'b0;
    #200 $finish;
  end
endmodule
